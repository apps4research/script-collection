#!/bin/bash

## This script adds an ID3 image "folder.jpg" to mp3 files recursivley in a 
## a current folder using ffmpeg. First it creates a copy of the mp3 files,
## with a prefix "out-", then it deletes the old mp3 files which do not have
## the prefix and then it renames the new mp3 files by removing the temporary
## prefix. It's primitive and has no safety hooks. It takes about 2 min for
## 20 mp3 files in a given folder, and is a little faster than using lame.

START_TIME=$SECONDS

for file in *.mp3
do
   ffmpeg -loglevel panic -i "$file" -i folder.jpg -map_metadata 0 -map 0 -map 1 out-"${file#./}"
done

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "Added album art in $(($ELAPSED_TIME/60)) min $(($ELAPSED_TIME%60)) secs"   

## now delete old files, exept those prefixed with "out-", the folder.jpg and any playlists
shopt -s extglob
rm -v !(out-*.mp3|folder.jpg|*.m3u)
shopt -u extglob

echo "Deleted old mp3 files"

## now rename files
for file in out-*.mp3;
do
    mv "$file" "${file#out-}"
done

echo "Renamed new files"