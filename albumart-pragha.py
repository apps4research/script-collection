#! /usr/bin/env python
import os, dbus, psutil, string
from shutil import copyfile
from urllib.parse import unquote

debug = 0

def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;
    
def words_in_string(word_list, a_string):
    return set(word_list).intersection(a_string.split())


if checkIfProcessRunning('pragha'):
	sb = dbus.SessionBus()
	rb_obj = sb.get_object("org.mpris.MediaPlayer2.pragha", "/org/mpris/MediaPlayer2")
	props_int = dbus.Interface(rb_obj, "org.freedesktop.DBus.Properties")
	rb_meta_dict = props_int.Get("org.mpris.MediaPlayer2.Player", "Metadata")
	source = rb_meta_dict["xesam:url"]

	if (debug == 0):
		if 'xesam:title' in rb_meta_dict:
			
			radio = False
			if (source[:4] == "http"): radio = True # check if its a radio station (stream)

			if (radio):
				#http://swr-swr1-bw.cast.addradio.de/swr/swr1/bw/mp3/128/stream.mp3
				#http://stream.antenne1.de/a1stg/livestream2.mp3
				#http://swr-dasding-live.cast.addradio.de/swr/dasding/live/mp3/128/stream.mp3
				#http://swr-swr2-live.cast.addradio.de/swr/swr2/live/mp3/256/stream.mp3
				#https://swr-swr3-live.cast.addradio.de/swr/swr3/live/aac/96/stream.aac
				#http://swr-swr4-bw.cast.addradio.de/swr/swr4/bw/mp3/128/stream.mp3
				
				#format output for radio station
				if "swr1" in source:
					title = "SWR 1 Baden-Württemberg"
					cover = "swr1.jpg"
				elif "swr2" in source:
					title = "SWR 2"
					cover = "swr2.jpg"
				elif "swr3" in source:
					title = "SWR 3"
					cover = "swr3.jpg"
				elif "swr4" in source:
					title = "SWR 4 Baden-Württemberg"
					cover = "swr4.jpg"
				elif "dasding" in source:
					title = "DASDING Live"
					cover = "dasding.jpg"
				elif "antenne1" in source:
					title = "Antenne 1"
					cover = "antenne1.jpg"
				
				artists = string.capwords(rb_meta_dict["xesam:title"])
				album = "Radio Stream"
				radio_logo = os.path.expanduser('~')+"/Scripts/images/"+cover
			else:
				#format output for mp3 file
				year = rb_meta_dict["xesam:contentCreated"]
				title = rb_meta_dict["xesam:title"]
				artists = ", ".join(rb_meta_dict["xesam:artist"])
				album = rb_meta_dict["xesam:album"] + " (" + year + ")"
				
			if (len(title) > 40): title = title[:40]+"…"
			if (len(artists) > 40): artists = artists[:40]+"…"
			if (len(album) > 33): album = album[:33]+"…"
			
			f = open(os.path.expanduser('~')+"/Scripts/nowplaying.txt", "w")
			f.write("${offset 85}"+title+"\n${offset 85}"+artists+"\n${offset 85}"+album)
			
			if 'mpris:artUrl' in rb_meta_dict:
				cover_art_url = unquote(rb_meta_dict["mpris:artUrl"])
				f.write("\n"+cover_art_url[7:])
			else:
				if (radio):
					f.write("\n"+radio_logo)
				else:
					f.write("\n"+os.path.expanduser('~')+'/Scripts/images/album_nocover.jpg')

			f.close
	else:
		print(rb_meta_dict)
