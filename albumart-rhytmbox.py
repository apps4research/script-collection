#! /usr/bin/env python
import os
import dbus
import psutil
from shutil import copyfile

def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;

if checkIfProcessRunning('rhythmbox'):
	sb = dbus.SessionBus()
	rb_obj = sb.get_object("org.mpris.MediaPlayer2.rhythmbox", "/org/mpris/MediaPlayer2")
	props_int = dbus.Interface(rb_obj, "org.freedesktop.DBus.Properties")
	rb_meta_dict = props_int.Get("org.mpris.MediaPlayer2.Player", "Metadata")

	#info
	#title = rb_meta_dict["xesam:title"]
	#artists = ", ".join(rb_meta_dict["xesam:artist"]) # track can have more than one artist apparently
	#album = rb_meta_dict["xesam:album"]
	if 'mpris:artUrl' in rb_meta_dict:
		cover_art_url = rb_meta_dict["mpris:artUrl"]
		copyfile(os.path.expanduser('~')+cover_art_url[17:], os.path.expanduser('~')+'/Scripts/images/album_cover.jpg')
	else:
		copyfile(os.path.expanduser('~')+'/Scripts/images/album_nocover.jpg', os.path.expanduser('~')+'/Scripts/images/album_cover.jpg')
