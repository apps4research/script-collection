#!/bin/bash
#first play a sound and then take a screenshot of active windows and 
#save it to user pictures directory
what=$1
if [ $what == "window" ]; then
  play -q ~/Scripts/sounds/shutter.wav
  scrot -bu -d 5 -c 'window_%S_%d%b%Y_$wx$h.png' -e 'mv $f $$(xdg-user-dir PICTURES)/Screenshots'
else
  scrot -b 'desktop_%S_%d%b%Y_$wx$h.png' -e 'mv $f $$(xdg-user-dir PICTURES)/Screenshots'
fi
